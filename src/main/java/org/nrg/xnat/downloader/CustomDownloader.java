package org.nrg.xnat.downloader;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.json.*;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.XDATItem;
import org.nrg.xdat.bean.CatCatalogBean;
import org.nrg.xdat.bean.CatEntryBean;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.model.XnatImagesessiondataI;
import org.nrg.xdat.model.CatEntryI;
import org.nrg.xdat.model.XnatAbstractresourceI;
import org.nrg.xdat.om.XnatAbstractresource;
import org.nrg.xdat.om.XnatExperimentdata;
import org.nrg.xdat.om.XnatImagescandata;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.nrg.xdat.om.XnatSubjectdata;
import org.nrg.xft.XFTItem;
import org.nrg.xft.presentation.FlattenedItemA;
import org.nrg.xft.presentation.FlattenedItemA.HistoryConfigI;
import org.nrg.xft.presentation.ItemJSONBuilder;
import org.nrg.xft.security.UserI;
import org.nrg.xnat.utils.CatalogUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.nrg.config.entities.Configuration;
import org.nrg.config.services.ConfigService;

public class CustomDownloader {
	private static final Logger LOGGER = LoggerFactory.getLogger(CustomDownloader.class);

	public static Path doDownload(String projectId, String experimentId, String rulesetId, UserI user) throws Exception {
		
		Map<String, String> parameters = new HashMap<String, String>();
		parameters.put("experimentID", experimentId);
		parameters.put("project", projectId);

		String directory = Files.createTempDirectory("downloader").toString();

		// PARSE RULES
		// Fetch download rule script
		ConfigService configService = XDAT.getConfigService();
		Configuration ruleset = configService.getConfig("downloader", "rulesets/" + rulesetId);
		String rawscript = ruleset.getContents();
		
		// Parse download rule script
		JSONObject rules =  new JSONObject(rawscript);
		System.out.println(rules.toString());
		
		// Global variables
		String key = rules.getString("key");
		System.out.println(key);
		
		// Per series rules
		JSONArray series = rules.getJSONArray("series");
		Map<String, JSONObject> downloadrules = new HashMap<String, JSONObject>();
	
		for (int i = 0; i < series.length(); ++i) {
			JSONObject currentSeries = series.getJSONObject(i);
			
			// Get key
			String serieskey = currentSeries.getString(key);
			
			// Put remaining rules for series in rule list
			downloadrules.put(serieskey, currentSeries);
		}
		
		// POPULATE SESSION-LEVEL METADATA
		Map<String, String> sessionMetadata = new HashMap<String, String>();
		sessionMetadata.put("sessionID", parameters.get("experimentID"));
		
		if (sessionMetadata.get("sessionID") == null) {
			System.out.println("Session ID was null");
		}
		
		XnatExperimentdata session = XnatExperimentdata.getXnatExperimentdatasById(sessionMetadata.get("sessionID"), user, false);
		
		if (session == null) {
			System.out.println("Session was null");
		}
		
		XnatProjectdata project = session.getPrimaryProject(false);
		XFTItem xftSession = session.getItem();
		ItemJSONBuilder ijb = new ItemJSONBuilder();
		HistoryConfigI noHistory = new FlattenedItemA.HistoryConfigI(){
			@Override
			public boolean getIncludeHistory() {
				return false;
			}
		};
		
		JSONObject sessionJsonObj = ijb.call(xftSession, noHistory, false);
		
		JSONObject sessionMetadataObj = sessionJsonObj.getJSONArray("items").getJSONObject(0).getJSONObject("data_fields");
		
		sessionMetadata.put("project", sessionMetadataObj.getString("project"));
		sessionMetadata.put("sessionLabel", sessionMetadataObj.getString("label"));
		//sessionMetadata.put("sessionDate", sessionMetadataObj.getString("date"));
		//sessionMetadata.put("sessionTime", sessionMetadataObj.getString("time"));
		//sessionMetadata.put("sessionModality", sessionMetadataObj.getString("modality"));
		sessionMetadata.put("subjectID", sessionMetadataObj.getString("subject_ID"));
		
		XnatSubjectdata subject = XnatSubjectdata.getXnatSubjectdatasById(sessionMetadataObj.getString("subject_ID"), user, false);
		XFTItem xftSubject = subject.getItem();
		
		JSONObject subjectJsonObj = ijb.call(xftSubject, noHistory, false);
		JSONObject subjectMetadataObj = subjectJsonObj.getJSONArray("items").getJSONObject(0).getJSONObject("data_fields");
		
		sessionMetadata.put("subjectLabel", subjectMetadataObj.getString("label"));
		
		System.out.println(sessionMetadata.toString());
		
		// Resolve paths in global variables
		String rootDirectory = rules.has("root_directory") ? handleFormatString(rules.getString("root_directory"), sessionMetadata) : null;
		String outputFileName = handleFormatString(rules.getString("filename"), sessionMetadata);
		
		// GET SERIES FROM REST FOR SESSION
		XnatImagesessiondata imageSession = XnatImagesessiondata.getXnatImagesessiondatasById(sessionMetadata.get("sessionID"), user, false);
		java.util.List<XnatImagescandataI> scanlist = imageSession.getScans_scan();
		
		Map<String, JSONObject> rulesforseries = new TreeMap<String, JSONObject>();
		Map<String, Integer> instanceCount = new HashMap<String, Integer>(); // Count instance number for each series
		String outputFile = directory + outputFileName;
		FileOutputStream fos  = new FileOutputStream(outputFile);
		ZipOutputStream zos = new ZipOutputStream(fos);
			
		// Go through list series on session being downloaded and find matches in rules
		for (int i = 0; i < scanlist.size(); ++i) {
			XnatImagescandataI thisscan = scanlist.get(i);
			String identifier = "";
			
			if (key.equals("series_description")) {
				identifier = thisscan.getSeriesDescription();
			} else if (key.equals("type")) {
				identifier = thisscan.getType();
			} else {
				System.out.println("Unknown key '" + key + "'");
			}
			
			System.out.println(identifier);
			
			JSONObject sdrules = downloadrules.get(identifier);
			System.out.println(rulesforseries.toString());
			
			if (sdrules == null || sdrules.length() == 0) {
				System.out.println("EXCLUDE");
			} else {
				Integer instanceNumber = instanceCount.get(identifier);
				
				if (instanceNumber == null) {
					instanceNumber = 1;
					instanceCount.put(identifier, instanceNumber);
				} else {
					instanceCount.put(identifier, instanceNumber + 1);
				}
				
				System.out.println("Instance count: " + instanceCount.toString());
				System.out.println("Instance number: " + instanceNumber);
				
				// Get scan metadata
				Map<String, String> scanMetadata = new HashMap<String, String>();
				scanMetadata.putAll(sessionMetadata);
				scanMetadata.put("scanID", thisscan.getId());
				scanMetadata.put("seriesDescription", thisscan.getSeriesDescription());
				scanMetadata.put("scanType", thisscan.getType());
				//scanMetadata.put("scanStartTime", scanMetadataObj.getString("startTime"));
				scanMetadata.put("instanceNumber", instanceCount.get(identifier).toString());
				
				// Get required resources
				List<String> requiredResources = Arrays.asList(sdrules.getString("resources").split(","));
				List<XnatAbstractresourceI> scanResources = thisscan.getFile();
				List<XnatAbstractresource> resourcesToDownload = new ArrayList<XnatAbstractresource>();
				
				for (XnatAbstractresourceI f : scanResources) {
					XnatAbstractresource r = XnatAbstractresource.getXnatAbstractresourcesByXnatAbstractresourceId(f.getXnatAbstractresourceId(), user, false);
					System.out.println(r.getLabel());
					
					if (requiredResources.contains(r.getLabel())) {
						resourcesToDownload.add(r);
						System.out.println("added " + thisscan.getType() + " " + r.getLabel() + " to download list");
					}
				}
				
				// Create directory to download this series to
				String destination = handleFormatString(sdrules.getString("destination"), scanMetadata).replace("/", "\\");

				if (rootDirectory != null) {
					destination = rootDirectory.replace("/", "\\") + destination;
				}
				
				String saveTo = directory + destination;
				
				System.out.println(saveTo);
				
				Path saveToPath = Paths.get(saveTo);
				Files.createDirectories(saveToPath);
				
				// Go through each resource and download it
				for (XnatAbstractresource res : resourcesToDownload) {
					System.out.println(res.getItem().getXSIType());
					XnatResourcecatalog catalog = (XnatResourcecatalog) res;
					String rootArchivePath = project.getRootArchivePath();
					
					CatCatalogBean catBean = catalog.getCleanCatalog(rootArchivePath, false, null, null);
					final String parentPath = catalog.getCatalogFile(rootArchivePath).getParent();
					
					ArrayList<CatEntryI> entries = new ArrayList<>();
					
					List<File> fileList = CatalogUtils.getFiles(catBean, parentPath);

					for (File file : fileList) {
						String fname = file.getName();
						
						// Creates a zip entry.
						String name = destination + fname;
						System.out.println("Compressing: " + saveTo + fname + " as " + name);
						ZipEntry zipEntry = new ZipEntry(name);
						zos.putNextEntry(zipEntry);
						
						// Read file content and write to zip output stream.
						FileInputStream fis = new FileInputStream(file.getAbsolutePath());
						byte[] buffer = new byte[8192];
						
						int length;
		                while ((length = fis.read(buffer)) > 0) {
		                    zos.write(buffer, 0, length);
		                }
		                
		                // Close the zip entry and the file input stream.
		                zos.closeEntry();
		                fis.close();
					}
				}
			}
			
			/*
			// Check for supplement
			if(!ser.getValue().isNull("supplements")) {
				JSONArray supplements = ser.getValue().getJSONArray("supplements");
				
				for (int i = 0; i < supplements.length(); ++i) {
					JSONObject supplement = supplements.getJSONObject(i);
					
					String supplementUri = handleFormatString(supplement.getString("URI"), scanMetadata);
					String supplementFile = handleFormatString(supplement.getString("filename"), scanMetadata);
					
					UploadUtils.downloadFile(supplementUri, Paths.get(saveTo + supplementFile));
					
					// Zip the file
					String name = destination + supplementFile;
					System.out.println("Compressing series " + ser.getKey() + ": " + saveTo + supplementFile + " as " + name);
					ZipEntry zipEntry = new ZipEntry(name);
					zos.putNextEntry(zipEntry);
					
					// Read file content and write to zip output stream.
					FileInputStream fis = new FileInputStream(saveTo + supplementFile);
					byte[] buffer = new byte[8192];
					
					int length;
	                while ((length = fis.read(buffer)) > 0) {
	                    zos.write(buffer, 0, length);
	                }
	                
	                // Close the zip entry and the file input stream.
	                zos.closeEntry();
	                fis.close();
				}
			}*/
		}
	
		// Check for session-level supplement
		if(!rules.isNull("supplements")) {
			JSONArray supplements = rules.getJSONArray("supplements");
			
			for (int i = 0; i < supplements.length(); ++i) {
				JSONObject supplement = supplements.getJSONObject(i);
				
				String supplementUri = handleFormatString(supplement.getString("URI"), sessionMetadata);
				String supplementFile = handleFormatString(supplement.getString("filename"), sessionMetadata);
				
				// Decode file name and resource
				int resourcesIndex = supplementUri.indexOf("resources");
				int filesIndex = supplementUri.indexOf("files");
				
				String resourcesComponent = supplementUri.substring(resourcesIndex, filesIndex);
				String filesComponent = supplementUri.substring(filesIndex);
				
				String resourceId = resourcesComponent.split("/")[1];
				String fileId = filesComponent.split("/")[1];
				
				List<XnatAbstractresourceI> sessionResources = imageSession.getAllResources();
				XnatAbstractresource sessionResourceToDownload;
				/*
				for (XnatAbstractresourceI f : sessionResources) {
					XnatAbstractresource r = XnatAbstractresource.getXnatAbstractresourcesByXnatAbstractresourceId(f.getXnatAbstractresourceId(), user, false);
					System.out.println(r.getLabel());
					
					if (sessionResourceToDownload.equals(r.getLabel())) {
						sessionResourceToDownload = r;
						System.out.println("added " + r.getLabel() + " to download list");
					}
				}
				
				//UploadUtils.downloadFile(supplementUri, Paths.get(saveTo + supplementFile));
				
				// Zip the file
				String name = destination + supplementFile;
				System.out.println("Compressing supplement " + ser.getKey() + ": " + saveTo + supplementFile + " as " + name);
				ZipEntry zipEntry = new ZipEntry(name);
				zos.putNextEntry(zipEntry);
				
				// Read file content and write to zip output stream.
				FileInputStream fis = new FileInputStream(saveTo + supplementFile);
				byte[] buffer = new byte[8192];
				
				int length;
	            while ((length = fis.read(buffer)) > 0) {
	                zos.write(buffer, 0, length);
	            }
	            */
	            
	            // Close the zip entry and the file input stream.
	            zos.closeEntry();
	            //fis.close();
			}
		}
		
		zos.close();
		fos.close();
		
		System.out.println("Done.");
		
		// DOWNLOAD DATA TO CACHE
		//String subjectLookupUri = "data/archive/subjects?columns=group&label=" + subject + "&project=" + project + "&format=csv";
	
		return Paths.get(outputFile);//.toFile();
	}

	private static String handleFormatString(String input, Map<String, String> values) {
		String result = input;
		
		for (Entry<String, String> v : values.entrySet()) {
			//String token = "{" + v.getKey() + "}";
			String token = "{" + v.getKey();
			
			//System.out.println(token);
			//System.out.println(result);
			
			/*if (result.contains(token)) {
				result = result.replace(token, v.getValue());
				
				System.out.println(result);
			}*/
			
			int matchIndex = result.indexOf(token);
			if (matchIndex != -1) {
				int nextBracket = result.indexOf("}", matchIndex);
				String fullToken = result.substring(matchIndex, nextBracket + 1);
				String value = v.getValue();
				
				if (v.getKey().toLowerCase().contains("date")) {
					String instructions = "";
					if (fullToken.contains(":")) {
						instructions = fullToken.split(":")[1].replaceAll("}", "");
					} else {
						instructions = "yyyyMMdd"; // default date format
					}
					
					// handle date format
					SimpleDateFormat dateIn = new SimpleDateFormat("yyyy-MM-dd");
					SimpleDateFormat dateOut = new SimpleDateFormat(instructions);
					
					try {
						Date date = dateIn.parse(value);
						value = dateOut.format(date);
					} catch (ParseException e) {
						// Leave value unparsed if parsing fails
					}
				} else if (v.getKey().toLowerCase().contains("time")) { 
					String instructions = "";
					if (fullToken.contains(":")) {
						instructions = fullToken.split(":")[1].replaceAll("}", "");
					} else {
						instructions = "HHmmss"; // default time format
					}
					
					// handle time format
					SimpleDateFormat timeIn = new SimpleDateFormat("HH:mm:ss");
					SimpleDateFormat timeOut = new SimpleDateFormat(instructions);
					
					try {
						Date time = timeIn.parse(value);
						value = timeOut.format(time);
					} catch (ParseException e) {
						// Leave value unparsed if parsing fails
					}
				} else if (fullToken.contains(":")) {
					System.out.println(fullToken + " has colon!");
					String instructions = fullToken.split(":")[1].replaceAll("}", "");
					
					String[] instr = instructions.split("-");
					int substringStart = Integer.parseInt(instructions.split("-")[0]);

					if (instr.length == 1) {
						value = value.substring(substringStart);
					} else {
						int substringEnd = Integer.parseInt(instructions.split("-")[1]);
						
						value = value.substring(substringStart, substringEnd);
					}
				} else {
					value = v.getValue();
				}
				
				result = result.replace(fullToken, value);
				
				System.out.println(result);
			}
		}
		
		System.out.println(result);
		
		return result;
	}
}
