package org.nrg.xnat.downloader.rest;

import io.swagger.annotations.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Path;

import org.apache.commons.io.IOUtils;
import org.nrg.framework.annotations.XapiRestController;
import org.nrg.xapi.rest.AbstractXapiRestController;
import org.nrg.xapi.rest.XapiRequestMapping;
import org.nrg.xdat.security.services.RoleHolder;
import org.nrg.xdat.security.services.UserManagementServiceI;
import org.nrg.xft.security.UserI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;
import org.nrg.xnat.downloader.CustomDownloader;

@Api(description = "XNAT Custom Downloader API")
@XapiRestController
@RequestMapping(value = "/downloader")
public class CustomDownloaderApi extends AbstractXapiRestController {

	private static final Logger LOGGER = LoggerFactory.getLogger(CustomDownloaderApi.class);
	
	@Autowired
	protected CustomDownloaderApi(UserManagementServiceI userManagementService, RoleHolder roleHolder) {
		super(userManagementService, roleHolder);
	}
	
    @ApiOperation(value = "Returns a zipped download packaged according to the given ruleset.")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Get successful."),
            @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
            @ApiResponse(code = 403, message = "Only site admins may access this resource."),
            @ApiResponse(code = 500, message = "Unexpected error.")
    })
	//@XapiRequestMapping(value = "rulesets/{ruleSetId}/projects/{projectId}/experiments/{experimentId}",  produces = "application/zip", method = RequestMethod.GET)
    @XapiRequestMapping(value = "rulesets/{ruleSetId}/projects/{projectId}/experiments/{experimentId}", produces = "application/zip", method = RequestMethod.GET)
    public ResponseEntity<StreamingResponseBody> getDownloadPackage(@ApiParam("The dowloader rule set ID.") @PathVariable final String ruleSetId,
    //public ResponseEntity<InputStreamResource> getDownloadPackage(@ApiParam("The dowloader rule set ID.") @PathVariable final String ruleSetId,
                                                       @ApiParam("The project ID.") @PathVariable final String projectId,
                                                       @ApiParam("The experiment ID.") @PathVariable final String experimentId) {
    	UserI user = getSessionUser();
    	//UploadUtils.initialize(user, false);
    	
    	//XnatImagesessiondata x = (XnatImagesessiondata) XnatExperimentdata.GetExptByProjectIdentifier(projectId, experimentId, user, false);
    	try {
    		final File download = CustomDownloader.doDownload(projectId, experimentId, ruleSetId, user).toFile();
    		//final Path download = CustomDownloader.doDownload(projectId, experimentId, ruleSetId, user);
    		//return zipResponse(download, download.getFileName().toString());

    		StreamingResponseBody srb = new StreamingResponseBody() {
    			@Override
    			public void writeTo(OutputStream out) throws IOException {
    				FileInputStream fis = new FileInputStream(download);
    				
    				try {
    					FileCopyUtils.copy(fis, out);
    					//IOUtils.copy(fis, out);
    				} finally {
    					fis.close();
    				}
    			}
    		};
    		
    		return ResponseEntity.ok()
                    .header(HttpHeaders.CONTENT_TYPE, "application/zip")
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + download.getName() + ".zip\"")
                    .header(HttpHeaders.CONTENT_LENGTH, Long.toString(download.length()))
                    .body(srb);
    	} catch (IOException e) {
			LOGGER.error("Custom Downloader Bad IO", e);
			e.printStackTrace();
    		System.out.println(e.getStackTrace());
			return null;
			//throw new NoContentException("Downloader IO error:\n" + e + "\n" + ExceptionUtils.getStackTrace(e));
		} catch (Exception e) {
			LOGGER.error("Custom Downloader Exception", e);
			e.printStackTrace();
			System.out.println(e.getStackTrace());
			return null;
			//throw new NoContentException("Downloader error:\n" + e + "\n" + ExceptionUtils.getStackTrace(e));
	    }
    }

	private ResponseEntity<InputStreamResource> zipResponse(Path zippedFile, String downloadedName) throws FileNotFoundException {
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentLength(zippedFile.toFile().length());
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        headers.set("content-disposition", "attachment; filename=" + downloadedName);
        return ResponseEntity.ok().headers(headers).body(new InputStreamResource(new FileInputStream(zippedFile.toFile())));
    }
}
