package org.nrg.xnat.downloader.plugin;

import org.nrg.framework.annotations.XnatPlugin;
import org.springframework.context.annotation.ComponentScan;

@XnatPlugin(value = "customDownloaderPlugin", name = "XNAT Custom Downloader Plugin",
description = "This plugin allows users to download imaging data by packaging it with custom rulesets.", version = "1.0")
@ComponentScan({"org.nrg.xnat.downloader.rest"})
public class CustomDownloaderPlugin {

}
